package mobileapp.practice6_4;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    ////////////EX 6_3 LocalFileSve ///////////////////////////////
    public void onButtonClick(View v){
        Toast msg = null;

        //파일 이름을 쓰는 editText2 가져오기
        EditText txt = (EditText) findViewById(R.id.editText2);
        String inputName = txt.getText().toString(); //파일이름을 가져온다.

        //파일 내용 editText1 가져오기
        EditText txt2 = (EditText) findViewById(R.id.editText);
        String input2 = txt2.getText().toString();

            switch(v.getId()){
                case R.id.button:

                    try{
                        FileOutputStream fos = openFileOutput(inputName, Context.MODE_PRIVATE);
                        fos.write(input2.getBytes());
                        fos.close();
                    }catch (FileNotFoundException e){
                        msg = Toast.makeText(this,
                                "파일 생성 실패 \n 파일을 찾을 수 없음", Toast.LENGTH_SHORT);
                        e.printStackTrace();
                        break;
                    }catch (IOException e){
                        msg = Toast.makeText(this,
                                "파일 생성 실패 \n 내용을 찾을 수 없음", Toast.LENGTH_SHORT);
                        e.printStackTrace();
                        break;
                    }
                    msg = Toast.makeText(this,
                            "파일 생성 성공", Toast.LENGTH_SHORT);

                    //파일 생성 이후
                    //키보드를 감춘다
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(findViewById(R.id.editText).getWindowToken(), 0);
                    break;


                case R.id.button2:
                    TextView view = (TextView) findViewById(R.id.textView);

                    try{
                        FileInputStream fis = openFileInput(inputName);
                        byte[] buff =  new byte[fis.available()];
                        while(fis.read(buff) != -1) {;
                            }
                        fis.close();
                        view.setText(new String(buff));
                    }catch (FileNotFoundException e){
                        msg = Toast.makeText(this,
                                "파일 읽기 실패 \n 파일을 찾을 수 없음", Toast.LENGTH_SHORT);
                        e.printStackTrace();
                        break;
                    }catch (IOException e){
                        msg = Toast.makeText(this,
                                "파일 읽기 실패 \n 내용을 찾을 수 없음", Toast.LENGTH_SHORT);
                        e.printStackTrace();
                        break;
                    }
                    msg = Toast.makeText(this,
                            "파일 읽기 성공", Toast.LENGTH_SHORT);
                    break;


                case R.id.button3:
                    if(deleteFile(inputName)){
                        msg = Toast.makeText(this,
                                "파일 삭제 성공", Toast.LENGTH_SHORT);
                    }else {
                        msg = Toast.makeText(this,
                                "파일 삭제 실패", Toast.LENGTH_SHORT);
                    }
                    break;


                case R.id.button4:
                    String output = "파일 목록";
                    String[] list = fileList();
                    output += ": 파일 개수 " + list.length + "개";
                    for(String file : list){
                        output += "\n" ;
                        output += file ;
                    }
                    TextView view2 = (TextView) findViewById(R.id.textView2);
                    view2.setText(output);
                    msg = Toast.makeText(this,
                            "파일 목록 확인 성공", Toast.LENGTH_SHORT);
                    break;
            }
            msg.show();
    }
////////////EX 6_3 LocalFileSve //////////;./////////////////////





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
